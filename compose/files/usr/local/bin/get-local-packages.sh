#!/bin/bash
# uncomment if you need the quartet code itself
#git clone https://gitlab.com/serial-lab/qu4rtet.git
git clone https://gitlab.com/serial-lab/quartet_epcis.git
git clone https://gitlab.com/serial-lab/quartet_capture.git
git clone https://gitlab.com/serial-lab/quartet_output.git
git clone https://gitlab.com/serial-lab/quartet_masterdata.git
git clone https://gitlab.com/serial-lab/quartet_integrations.git
git clone https://gitlab.com/serial-lab/quartet_templates.git
git clone https://gitlab.com/serial-lab/quartet-vrs.git
git clone https://gitlab.com/serial-lab/quartet_trail.git
git clone https://gitlab.com/serial-lab/EPCPyYes.git
git clone https://gitlab.com/serial-lab/EParseCIS.git
git clone https://gitlab.com/serial-lab/gs123.git
git clone https://gitlab.com/serial-lab/serialbox.git
git clone https://gitlab.com/serial-lab/random-flavorpack.git
git clone https://gitlab.com/serial-lab/list_based_flavorpack.git
git clone https://gitlab.com/serial-lab/quartet_tracelink.git
git clone https://gitlab.com/serial-lab/quartet-4nt4r3s.git
git clone https://gitlab.com/serial-lab/quartet-sftp.git
git clone https://gitlab.com/serial-lab/third-party-flavors.git



