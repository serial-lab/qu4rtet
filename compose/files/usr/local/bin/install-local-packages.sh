#!/bin/bash

pip install -e ./quartet_epcis --no-deps
pip install -e ./quartet_capture --no-deps
pip install -e ./quartet_output --no-deps
pip install -e ./quartet_masterdata --no-deps
pip install -e ./quartet_integrations --no-deps
pip install -e ./quartet_templates --no-deps
pip install -e ./quartet-vrs --no-deps
pip install -e ./quartet_trail --no-deps
pip install -e ./EPCPyYes --no-deps
pip install -e ./EParseCIS --no-deps
pip install -e ./gs123 --no-deps
pip install -e ./serialbox --no-deps
pip install -e ./random-flavorpack --no-deps
pip install -e ./list_based_flavorpack --no-deps
pip install -e ./quartet_tracelink --no-deps
pip install -e ./quartet-4nt4r3s --no-deps
pip install -e ./quartet-sftp --no-deps
pip install -e ./third-party-flavors --no-deps
