from django.conf import settings
from django.urls import re_path, include
from django.conf.urls.static import static
from django.urls import path
from django.views import defaults as default_views
from rest_framework.schemas import get_schema_view
from qu4rtet.api.renderers import JSONOpenAPIRenderer
from qu4rtet.api import routers
from qu4rtet.api.views import APIRoot
from rest_framework import permissions
from drf_yasg.views import get_schema_view as yasg_get_shemea_view
from drf_yasg import openapi

from quartet_trail.urls import urlpatterns as trail_patterns

schema_view = get_schema_view(
    title="QU4RTET API", renderer_classes=[JSONOpenAPIRenderer]
)

yasg_schema_view = yasg_get_shemea_view(
    openapi.Info(
        title="QU4RTET API",
        default_version="v1",
        description="The QU4RTET API",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)
urlpatterns = [
    re_path(r"^$", APIRoot.as_view()),
    re_path(r"^schema/?", schema_view, name="schema"),
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        yasg_schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        yasg_schema_view.with_ui("swagger", cache_timeout=0),
        name="swagger",
    ),
    re_path(
        r"^redoc/$",
        yasg_schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    re_path(r"^capture/", include("quartet_capture.urls", namespace="quartet-capture")),
    re_path(r"^output/", include("quartet_output.urls", namespace="quartet-output")),
    re_path(r"^accounts/", include("allauth.urls")),
    re_path(r"^epcis/", include("quartet_epcis.urls")),
    re_path(r"^api-auth/", include("rest_framework.urls")),
    re_path(r"^serialbox/", include("serialbox.api.urls")),
    re_path(r"^masterdata/", include("quartet_masterdata.urls")),
    re_path(r"^templates/", include("quartet_templates.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += routers.urlpatterns
urlpatterns += trail_patterns
try:
    from .local_urls import urlpatterns as local_urlpatterns

    urlpatterns += local_urlpatterns
    print("LOCAL URLS FOUND")
except ImportError as ie:
    if "local_urls" not in str(ie):
        raise

if "django.contrib.admin" in settings.INSTALLED_APPS:
    from qu4rtet.admin import admin_site

    urlpatterns = [
        path(getattr(settings, "DJANGO_ADMIN_URL", "qu4rtetadmin/"), admin_site.urls),
    ] + urlpatterns

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        re_path(
            r"^400/$",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        re_path(
            r"^403/$",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        re_path(
            r"^404/$",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        re_path(r"^500/$", default_views.server_error),
    ]

    try:
        if "debug_toolbar" in settings.INSTALLED_APPS:
            import debug_toolbar

            urlpatterns = [
                re_path(r"^__debug__/", include(debug_toolbar.urls)),
            ] + urlpatterns
    except ImportError:
        print("Could not import the debug toolbar.")
