#!/bin/bash
find ../ -type d -name '*egg-info' -exec rm -r {} \;
poetry add --editable ../quartet_epcis
poetry add --editable ../quartet_capture
poetry add --editable ../quartet_output
poetry add --editable ../quartet_masterdata
poetry add --editable ../quartet_integrations
poetry add --editable ../quartet_templates
poetry add --editable ../quartet_vrs
poetry add --editable ../quartet_trail
poetry add --editable ../EPCPyYes
poetry add --editable ../EParseCIS
poetry add --editable ../gs123
poetry add --editable ../serialbox
poetry add --editable ../random-flavorpack
poetry add --editable ../list_based_flavorpack
poetry add --editable ../quartet_tracelink
poetry add --editable ../quartet_4nt4r3s
poetry add --editable ../qu4rtet_sftp
poetry add --editable ../third_party_flavors
