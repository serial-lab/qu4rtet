#!/bin/bash
# uncomment if you need the quartet code itself
#git clone https://github.com/rmagee/qu4rtet.git ../
git clone https://github.com/rmagee/quartet_epcis.git ../
git clone https://github.com/rmagee/quartet_capture.git ../
git clone https://github.com/rmagee/quartet_output.git ../
git clone https://github.com/rmagee/quartet_masterdata.git ../
git clone https://github.com/rmagee/quartet_manifest.git ../
git clone https://github.com/rmagee/quartet_integrations.git ../
git clone https://github.com/rmagee/quartet_templates.git ../
git clone https://github.com/rmagee/quartet_vrs.git ../
git clone https://github.com/rmagee/quartet_trail.git ../
git clone https://github.com/rmagee/EPCPyYes.git ../
git clone https://github.com/rmagee/EParseCIS.git ../
git clone https://github.com/rmagee/gs123.git ../
git clone https://github.com/rmagee/serialbox.git ../
git clone https://github.com/rmagee/random-flavorpack.git ../
git clone https://github.com/rmagee/list_based_flavorpack.git ../
git clone https://github.com/rmagee/quartet_tracelink.git ../

poetry add --group local ../quartet_epcis
poetry add --group local ../quartet_capture
poetry add --group local ../quartet_output
poetry add --group local ../quartet_masterdata
poetry add --group local ../quartet_integrations
poetry add --group local ../quartet_templates
poetry add --group local ../quartet_vrs
poetry add --group local ../quartet_trail
poetry add --group local ../EPCPyYes
poetry add --group local ../EParseCIS
poetry add --group local ../gs123
poetry add --group local ../serialbox
poetry add --group local ../random_flavorpack
poetry add --group local ../list_based_flavorpack
poetry add --group local ../quartet_tracelink





